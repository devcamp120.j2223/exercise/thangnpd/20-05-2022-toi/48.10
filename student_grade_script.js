const gNUMBER_COL = 0;
const gSTUDENT_COL = 1;
const gSUBJECT_COL = 2;
const gGRADE_COL = 3;
const gEXAM_DATE_COL = 4;
const gACTION_COL = 5;
var gColName = [
  'stt',
  'studentId',
  'subjectId',
  'grade',
  'examDate'
];

var gBASE_URL = "https://62454a477701ec8f724fb923.mockapi.io/api/v1/";

var gStudents; // tạo biến toàn cục chứa danh sách student
var gSubjects; // tạo biến toàn cục chứa danh sách môn học
var gGrades; // tạo biến toàn cục chứa danh sách bảng điểm
var gGradesFilter; //tạo biến toàn cục chứa danh sách bảng điểm sau khi filter 
var gGradeId; // tạo biến chứa id của 1 grade reocrd, dùng cho việc update grade record

function callApiGetStudent() {
  "use-strict"
  $.ajax({
    url: gBASE_URL + "/students",
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (response) {
      gStudents = response; // trả về thành công thì gán vào biến
      loadStudentToSelect(gStudents); // gọi hàm render student vô select
    },
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function callApiGetSubject() {
  "use-strict"
  $.ajax({
    url: gBASE_URL + "/subjects",
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (response) {
      gSubjects = response; // trả về thành công thì gán vào biến
      loadSubjectToSelect(gSubjects); // gọi hàm render subject vô select
    },
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function callApiGetAllGrade() {
  "use-strict"
  $.ajax({
    url: gBASE_URL + "/grades",
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (response) {
      gGradesFilter = response;
      gGrades = response; // trả về thành công thì gán vào biến
      loadGradeToTable(gGrades); // gọi hàm render grade vô table
    },
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function loadStudentToSelect(paramStudents) {
  for (var bI = 0; bI < paramStudents.length; bI++) { // lấy option từ array cho vào select student
    $('<option/>').val(paramStudents[bI].id)
      .text(paramStudents[bI].firstname + " " + paramStudents[bI].lastname)
      .appendTo('#gRoleSelectStudent');
  }
  for (var bI = 0; bI < paramStudents.length; bI++) { // lấy option từ array cho vào select student
    $('<option/>').val(paramStudents[bI].id)
      .text(paramStudents[bI].firstname + " " + paramStudents[bI].lastname)
      .appendTo('#create-grade-student');
  }
  for (var bI = 0; bI < paramStudents.length; bI++) { // lấy option từ array cho vào select student
    $('<option/>').val(paramStudents[bI].id)
      .text(paramStudents[bI].firstname + " " + paramStudents[bI].lastname)
      .appendTo('#update-grade-student');
  }
}

function loadSubjectToSelect(paramSubjects) {
  for (var bI = 0; bI < paramSubjects.length; bI++) { // lấy option từ array cho vào select student
    $('<option/>').val(paramSubjects[bI].id)
      .text(paramSubjects[bI].subjectName)
      .appendTo('#gRoleSelectSubject');
  }
  for (var bI = 0; bI < paramSubjects.length; bI++) { // lấy option từ array cho vào select student
    $('<option/>').val(paramSubjects[bI].id)
      .text(paramSubjects[bI].subjectName)
      .appendTo('#create-grade-subject');
  }
  for (var bI = 0; bI < paramSubjects.length; bI++) { // lấy option từ array cho vào select student
    $('<option/>').val(paramSubjects[bI].id)
      .text(paramSubjects[bI].subjectName)
      .appendTo('#update-grade-subject');
  }
}

function getStudentNameFromId(paramStudentId) { // lấy id student để kiếm ra name, show vào bảng
  "use-strict"
  var vStudentName = "";
  for (var bI = 0; bI < gStudents.length; bI++) {
    if (gStudents[bI].id == paramStudentId) {
      vStudentName = gStudents[bI].firstname + " " + gStudents[bI].lastname;
    }
  }
  return vStudentName;
}

function getSubjectNameFromId(paramSubjectId) { // lấy id subject để kiếm ra subject name, show vào bảng
  "use-strict"
  var vSubjectName = "";
  for (var bI = 0; bI < gSubjects.length; bI++) {
    if (gSubjects[bI].id == paramSubjectId) {
      vSubjectName = gSubjects[bI].subjectName;
    }
  }
  return vSubjectName;
}

function loadGradeToTable(paramGrades) {
  "use-strict"
  $("#student-grade-table").DataTable({
    data: paramGrades,
    bDestroy: true, // fix error cannot reinitialise data table
    columns: [
      { data: gColName[gNUMBER_COL] },
      { data: gColName[gSTUDENT_COL] },
      { data: gColName[gSUBJECT_COL] },
      { data: gColName[gGRADE_COL] },
      { data: gColName[gEXAM_DATE_COL] },
      { data: gColName[gACTION_COL] }
    ],
    columnDefs: [
      {
        targets: gNUMBER_COL,
        render: function (data, type, row, meta) { //Render số thứ tự trong bảng
          return meta.row + 1;
        }
      },
      {
        targets: gSTUDENT_COL,
        render: getStudentNameFromId
      },
      {
        targets: gSUBJECT_COL,
        render: getSubjectNameFromId
      },
      {
        targets: gACTION_COL,
        defaultContent: `
        <i class="fas fa-edit icon-edit" title="Edit" style="cursor:pointer"></i>
        <i class="fas fa-trash icon-delete" title="Delete" style="cursor:pointer"></i>
      `
      }
    ]
  })
}

function createGrade(paramStudentId, paramSubjectId, paramScore, paramExamDate) {
  //B1: Kiểm tra dữ liệu
  var vCheck = validateCreateDataInput(paramStudentId, paramSubjectId, paramScore, paramExamDate);

  //B2: xử lý dữ liệu
  if (vCheck) {
    var vGradeObjRequest = {
      studentId: paramStudentId,
      subjectId: paramSubjectId,
      grade: paramScore,
      examDate: paramExamDate
    };
    callApiCreateGrade(vGradeObjRequest);
  }
}

function callApiCreateGrade(paramGradeObj) {
  "use-strict"
  $.ajax({
    url: gBASE_URL + "/grades",
    type: "POST",
    dataType: 'json',
    data: JSON.stringify(paramGradeObj),
    contentType: "application/json",
    async: false,
    success: function (response) {
      console.log("Tạo mới 1 grade thành công");
      $("#modal-create-grade").modal('hide');
      onPageLoading(); //load lại table, student, subject
    },
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function validateCreateDataInput(paramStudentId, paramSubjectId, paramScore, paramExamDate) {
  var vResult = true;
  if (paramStudentId == 'none') {
    vResult = false;
    alert("Chưa chọn sinh viên");
  }
  if (paramSubjectId == 'none') {
    vResult = false;
    alert("Chưa chọn môn học");
  }
  if (paramScore.trim() == '' || paramScore < 0 || paramScore > 10) {
    vResult = false;
    alert("Chưa nhập điểm, điểm chỉ được từ 1 đến 10");
  }
  if (paramExamDate.trim() == '') {
    vResult = false;
    alert("Chưa nhập ngày thi");
  }
  return vResult;
}

function filterGrade(paramStudentId, paramSubjectId) {
  //B1: thu thập dữ liệu
  var vStudentId = paramStudentId;
  var vSubjectId = paramSubjectId;

  //B2: xác thực dữ liệu, ko cần bước này vì có thể để trống ô chọn và ấn filter để filter ra tất cả record

  //B3: xử lý dữ liệu
  filterGradeTable(vStudentId, vSubjectId);
}

function filterGradeTable(paramStudentId, paramSubjectId) {
  var vResult = [];
  vResult = gGradesFilter.filter(function (vFilter) {
    return ((vFilter.studentId == paramStudentId || paramStudentId == "none") &&
      (vFilter.subjectId == paramSubjectId || paramSubjectId == "none"));
  });
  console.log(vResult);
  //B4: hiển thị dữ liệu
  if (vResult.length > 0) { //nếu dữ liệu filter có thì thực hiện việc đổ data vào table sau 
    console.log("Tìm dc " + vResult.length + " record khớp sau khi filter!");
    loadGradeToTable(vResult);
  } else { // ko tìm dc user nào hoặc input để rỗng thì render hết data ra lại
    loadGradeToTable(gGradesFilter);
  }
}

function onPageLoading() {
  callApiGetStudent();
  callApiGetSubject();
  callApiGetAllGrade();
}

function onBtnEditGradeClick(paramCurrentRow) {
  console.log("Nút chi tiết được nhấn");
  //Xác định thẻ tr là cha của nút được chọn
  var vThisRow = paramCurrentRow.closest('tr');
  //Lấy datatable row
  var vDatatableRow = $("#student-grade-table").DataTable().row(vThisRow);
  //Lấy data của dòng 
  var vGradeData = vDatatableRow.data();
  //Lưu GradeId vào biến
  gGradeId = vGradeData.id;

  //đổ data vào modal

  //dùng trigger('change') để lấy option đang dc chọn và hiển thị lên khi xài slelect2
  $("#update-grade-student").val(vGradeData.studentId).trigger('change');
  //dùng trigger('change') để lấy option đang dc chọn và hiển thị lên khi xài slelect2
  $("#update-grade-subject").val(vGradeData.subjectId).trigger('change');

  $("#input-grade").val(vGradeData.grade);
  $(".datetimepicker-input-2").val(vGradeData.examDate);

  //show modal ra
  $("#modal-student-detail").modal('show');
}

function onBtnUpdateGradeClick(paramStudentId, paramSubjectId, paramScore, paramExamDate) {
  console.log("Nút update dc nhấn");
  //B1: Kiểm tra dữ liệu
  var vCheck = validateCreateDataInput(paramStudentId, paramSubjectId, paramScore, paramExamDate);

  //B2: xử lý dữ liệu
  if (vCheck) {
    var vGradeObjRequest = {
      studentId: paramStudentId,
      subjectId: paramSubjectId,
      grade: paramScore,
      examDate: paramExamDate
    };
    callApiUpdateGrade(vGradeObjRequest);
  }
}

function callApiUpdateGrade(paramUpdateObj) {
  "use-strict"
  $.ajax({
    url: gBASE_URL + "/grades/" + gGradeId,
    type: "PUT",
    dataType: 'json',
    data: JSON.stringify(paramUpdateObj),
    contentType: "application/json",
    async: false,
    success: function (response) {
      console.log("Update grade thành công");
      $("#modal-student-detail").modal('hide');
      onPageLoading(); //load lại table, student, subject
    },
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function onBtnDeleteGradeClick(paramCurrentRow) {
  console.log("Nút delete 1 grade dc nhấn");
  //Xác định thẻ tr là cha của nút được chọn
  var vThisRow = paramCurrentRow.closest('tr');
  //Lấy datatable row
  var vDatatableRow = $("#student-grade-table").DataTable().row(vThisRow);
  //Lấy data của dòng 
  var vGradeData = vDatatableRow.data();
  //Lưu GradeId vào biến
  gGradeId = vGradeData.id;
  $("#delete-confirm-modal").modal('show');

}

function confirmDeleteGrade(paramGradeId) {
  $.ajax({
    url: gBASE_URL + "/grades/" + gGradeId,
    type: "DELETE",
    dataType: 'json',
    contentType: "application/json",
    async: false,
    success: function (response) {
      console.log("Delete grade thành công");
      $("#delete-confirm-modal").modal('hide');
      onPageLoading(); //load lại table, student, subject
    },
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

$(document).ready(function () {
  onPageLoading();

  $("#btn-filter-data").on('click', function () { //filter bảng điểm theo student và subject
    filterGrade($('#gRoleSelectStudent').val(), $('#gRoleSelectSubject').val());
  });

  $("#btn-add-student").on('click', function () { //tạo mới 1 revord bảng điểm
    $("#modal-create-grade").modal('show');
  });

  $("#btn-create-grade").on('click', function () { //tạo mới 1 record cho grade-student
    createGrade($('#create-grade-student').val(),
      $('#create-grade-subject').val(),
      $('#input-create-grade').val(),
      $('.datetimepicker-input').val()
    );
  });

  $("#student-grade-table").on('click', '.icon-edit', function () {
    onBtnEditGradeClick(this);
  });

  $("#btn-update-student").on('click', function () {
    onBtnUpdateGradeClick($("#update-grade-student").val(),
      $("#update-grade-subject").val(),
      $("#input-grade").val(),
      $(".datetimepicker-input-2").val()
    );
  });

  $("#student-grade-table").on('click', '.icon-delete', function () {
    onBtnDeleteGradeClick(this);
  });

  $("#btn-confirm-delete-student-grade").on('click', function(){
    confirmDeleteGrade(gGradeId);
  });
  
});

